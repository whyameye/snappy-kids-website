/* add listener which distinguishes between ie8 attachEvent and the rest of the world */
function addListener(element, event, fn) {

    // Use addEventListener if available
    if (element.addEventListener) {
	element.addEventListener(event, fn, false);
	
    // Otherwise use attachEvent, set this and event
    } else if (element.attachEvent) {
	element.attachEvent('on' + event, (function (el) {
            return function() {
		fn.call(el, window.event);
            };
	}(element)));
	
	// Break closure and primary circular reference to element
	element = null;
    }
}

// reload FB like and share buttons after ajax call
$(document).ajaxComplete(function() {
    FB.XFBML.parse();
});

/*

jQuery(function() {
    jQuery('#content').change(function(e) {
	console.log("content thingy called");
	//FB.XFBML.parse();
    });
})
*/
// IE6 alpha channel workaround for critical images:
function fixPngs() {
    var pngList = ['Text1.png', 'snap.png', 'Text2.png']
    // Loops through all img tags
    for(i = 0; i < document.images.length; i++) {
	var u = document.images[i].src;
	var o = document.images[i];
	for (j=0; j < pngList.length; j++) {
	    if(u.indexOf(pngList[j]) > 0) {
		o.attachEvent('onload', fixPng);
		o.src = u;
	    }
	}
    }
}

// u = url of the image
// o = image object
function fixPng(e) {
  var caller = e.target            ||
               e.srcElement        ||
               window.event.target ||
               window.event.srcElement;
  var u = caller.src;
  var o = caller;

  if(!o || !u)
    return;

  // Detach event so is not triggered by the AlphaImageLoader
  o.detachEvent('onload', fixPng);

  // Save original sizes
  var oldw = o.clientWidth;
  var oldh = o.clientHeight;

  // Need to give it an image so we don't get the red x
  o.src          = '/website/wp/x.gif';
  o.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+ u +"', sizingMethod='scale')";

  // Restore original sizes
  if(o.style) {
    o.style.width  = oldw +"px";
    o.style.height = oldh +"px";
  }
}
$('#reply-title').livequery(function() {
    var html = $('#reply-title').html();
    var title = html.substring(0,7);
    var theRest = html.substring(7)
    if (title === "Profile") {
	html = "Login to Comment:"+theRest;
	$('#reply-title').html(html);
	$('.social-divider').hide();
	$('.social-input-row').hide();
	$('.form-submit').hide();
    }
});

$('#post_to').livequery(function()
			{
			    $("label[for='post_to_service'").html("<input type='checkbox' name='post_to_service' id='post_to_service' value='1' checked> Also posting to Facebook");
			    //$('#post_to_service').prop('checked', true);
			}, function() {
			}
		       );
//html("Posting to Facebook");
// make the Facebook comments area responsive:
/*
$(".fb-comments").attr("data-width", $(".fb-comments").parent().width());
$(window).on('resize', function() {
    if (typeof(timeoutWaiting) !== 'undefined') {
	clearTimeout(timeoutWaiting);
    }
    facebookFails = 0;
    resizeFacebookComments();
});
*/
/*
var facebookFails = 0;

function resizeFacebookComments() {
    try {
	var src = $('.fb-comments iframe').attr('src').split('width=');
    }
    catch(e) {
	facebookFails += 1;
	if (facebookFails < 30) {
	    timeoutWaiting = setTimeout(resizeFacebookComments,100);
	}
	return;
    }
    facebookFails = 0;
    var width = $("h1").width(); // make comments area same width as comments title
    $('.fb-comments iframe')[0].contentWindow.location.replace(src[0] + 'width=' + width);
}
*/

f = new headerFunctions();
$(function() {
    if (typeof ie6 !== 'undefined') {
	fixPngs();
    }
    // set min height of content area based on sidebar height
    //$(".sidebar-inner").hide(); // FIXME hide sidebar when pages are shown
    if ($(".widget-area").css("float") !== "none") {
	var widgetHeight = $('.widget-area').height();
	$('#content').css("min-height", widgetHeight + 65); // 65 looks good...
    } else {
	$('#content').css("min-height", 0);
    }
    //resizeFacebookComments();
    //$('#post_to_service').prop('checked', true);
    //$('#post_to').html("Posting to Facebook");
});

$(window).on('resize', function() {
    if ($(".widget-area").css("float") !== "none") {
	var widgetHeight = $('.widget-area').height();
	$('#content').css("min-height", widgetHeight + 65); // 65 looks good...
    } else {
	$('#content').css("min-height", 0);
    }
});

function headerFunctions() { 

    var hotSpots = {};
    var canvas2DSupported = !!window.CanvasRenderingContext2D;
    //var canvas2DSupported = false;
    var onMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    var self = this;

    this.createHotspot = function(id, width, height) {
	hotSpots[id] = {};
	hotSpots[id].mode='inactive';
	if (canvas2DSupported) {
	    var canvas = document.createElement('canvas');
	    var ctx = canvas.getContext("2d");
	    canvas.width = width;
	    canvas.height = height;
	    hotSpots[id].canvas = canvas;
	    ctx.drawImage($('#'+id)[0],0,0);
	    if (buttonSelect === id) {
		this.initialSelect(id);
	    }
	}
    };

    this.hover = function(id) {
	if (onMobile || canvas2DSupported) {
	    return;
	}
	$('#'+id).stop();
	$('#'+id).fadeOut("slow");
	$('#'+id+"Selected").stop();
	$('#'+id+"Selected").fadeOut("slow");
	if (hotSpots[id].mode === "active") {
	    id += "ClickTo";
	} else {
	    id += "Hover";
	}
	$('#'+id).stop();
	$('#'+id).fadeIn("slow");
    };

    this.mouseout = function(id) {
	if (onMobile) {
	    return;
	}
	hotSpots[id].hover = false;
	$('#'+id+"Hover").stop();
	$('#'+id+"ClickTo").stop("slow");
	$('#'+id+"Hover").fadeOut("slow");
	$('#'+id+"ClickTo").fadeOut("slow");
	if (hotSpots[id].mode === "active") {
	    id += "Selected";
	}
	$('#'+id).stop();
	$('#'+id).fadeIn("slow");
    };

    this.unselectAll = function() {
	for (var hotSpot in hotSpots) {
	    if (hotSpots[hotSpot].mode === "active") {
		hotSpots[hotSpot].mode = "inactive";
		$('#'+hotSpot+"Selected").fadeOut("slow");
		$('#'+hotSpot).fadeIn("slow");
	    }
	}
}

    this.initialSelect = function(id) {
	id = (id === '' ? 'snapSays' : id);
	if (typeof hotSpots[id] !== 'undefined') {
	    hotSpots[id].mode = "active";
	    $('#'+id+"Selected").fadeIn(0, function() { $('#'+id).hide(); });
	}
    }

    this.touch = function(e, id) {
	calcOffset = function(element, offsetName) {
	    var offset = element[offsetName];
//	    while (typeof element.parentNode[offsetName] !== 'undefined') {
	    // FIXME: hack. Why 2? above should have worked instead?
	    for (var i = 0; i < 2; ++i) {
		element = element.parentNode;
		offset += element[offsetName];
	    }
	    return offset;
	};
	onMobile = true;
	var touch = e.changedTouches[0];
	//console.log("canvas offset "+ canvas.offsetLeft);
	var offsetX = touch.pageX - calcOffset(touch.target, 'offsetLeft');
	var offsetY = touch.pageY - calcOffset(touch.target, 'offsetTop');
	this.clickDo(e, id, offsetX, offsetY);
    };

    this.click = function(e, id) {
	if (!onMobile) {
            var offsetX = e.offsetX || e.layerX;
            var offsetY = e.offsetY || e.layerY;
	    this.clickDo(e, id, offsetX, offsetY);
	}
    };

    this.clickDo = function(e, id, offsetX, offsetY) {
	if (canvas2DSupported && !this.alpha(id, offsetX, offsetY)) {
	    return;
	}
	var testid = (id === 'snapSays' ? '' : id);
	//var testid = id; // "hello-world";
	loadPage(url+"/"+testid);
	//FIXME: hide sidebar on pages
	//$(".sidebar-inner").hide();
	this.unselectAll();
	hotSpots[id].mode = "active";
	$('#'+id).hide();
	$('#'+id+"ClickFrom").show();
	if (onMobile) {
	    $('#'+id+"Selected").stop();
	    $('#'+id+"Selected").fadeIn(0);
	} else {
	    $('#'+id+"Hover").hide();
	    $('#'+id+"ClickTo").show();
	}
	$('#'+id+"ClickFrom").fadeOut("slow");
    };

    this.mousemove = function(e, id) {
	if (onMobile || !canvas2DSupported) {
	    return;
	}
        var offsetX = e.offsetX || e.layerX;
        var offsetY = e.offsetY || e.layerY;
	var alpha = this.alpha(id, offsetX, offsetY);
	var hover = hotSpots[id].hover;
	if (alpha && !hover) {
	    hover = true;
	    hotSpots[id].hover = true;
	    $('#'+id).stop();
	    $('#'+id).fadeOut("slow");
	    $('#'+id+"Selected").stop();
	    $('#'+id+"Selected").fadeOut("slow");
	    if (hotSpots[id].mode === "active") {
		id += "ClickTo";
	    } else {
		id += "Hover";
	    }
	    $('#'+id).stop();
	    $('#'+id).fadeIn("slow");
	} else if (!alpha && hover) {
	    hover = false;
	    hotSpots[id].hover = false;

	    $('#'+id+"Hover").stop();
	    $('#'+id+"Hover").fadeOut("slow");
	    $('#'+id+"ClickTo").stop();
	    $('#'+id+"ClickTo").fadeOut("slow");
	    if (hotSpots[id].mode === "active") {
		id += "Selected";
	    }
	    $('#'+id).stop();
	    $('#'+id).fadeIn("slow");
	}
    }

    this.alpha = function(id, offsetX, offsetY) {
	var canvas = hotSpots[id].canvas;
	var ctx = canvas.getContext("2d");
	var imgWidth = $('#'+id).width();
	var imgHeight = $('#'+id).height();
	var x = offsetX * canvas.width / imgWidth,
	y = offsetY * canvas.height / imgHeight;
	var imgData=ctx.getImageData(x,y,1,1);
	return imgData.data[3];
    }
}
