<?php
  /**
   * The Header template for our theme
   *
   * Displays all of the <head> section and everything up till <div id="main">
   *
   * @package WordPress
   * @subpackage Twenty_Thirteen
   * @since Twenty Thirteen 1.0
   */
?><!DOCTYPE html>
<!--[if IE 6]>
  <script>var ie6 = true;</script>
<![endif]-->
<!--[if IE 7]>
  <html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
  <html class="ie ie8" <?php language_attributes(); ?>>
  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/ie-child.css" type="text/css" media="all" />
  <html xmlns:fb="http://www.facebook.com/2008/fbml"> <!-- for FB w/ IE8 -->
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
  <html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>	
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" rel="shortcut icon" type="image/x-icon" />
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width">
  <meta property="fb:app_id" content="294036490779045" />
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <!-- <script type="text/javascript" src="https://getfirebug.com/firebug-lite-debug.js"></script> -->
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
    <script type="text/javascript"> if (!window.console) console = {log: function() {}}; </script>
  <![endif]-->    
  <?php wp_head(); ?>
  </head>
  

  <body <?php body_class(); ?>>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.livequery.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/skFunctions.js"></script>
    <script>var url = "<?php bloginfo('url') ?>";</script>
    <div id="page" class="hfeed site">
      <header id="masthead" class="site-header" role="banner">
	<div class="home-link">
	  <img src="<?php echo get_stylesheet_directory_uri();?>/headerDummy.png"
	       width="100%" style="position:relative; opacity:0; filter: alpha(opacity=0)">
	  <?php
	  if ($_GET["ajax"] !== "y") {
	    addImgsFromJson("web2header.json", "img", true);
	  }
	  ?>
	</div>	
      </header><!-- #masthead -->

<!--     <div id="fred">
	<a href="<?php bloginfo('rss2_url'); ?>" title="RSS 2.0 Feed"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img2/rss_icon.png" width="24" height="24" alt="RSS 2 Feed" /></a>
      </div> -->

      <div id="main" class="site-main">