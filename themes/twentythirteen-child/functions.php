<?php
function twentythirteen_scripts_styles_child() {
	/*
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	 */
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	// Adds Masonry to handle vertical alignment of footer widgets.
	if ( is_active_sidebar( 'sidebar-1' ) )
		wp_enqueue_script( 'jquery-masonry' );

	// Loads JavaScript file with functionality specific to Twenty Thirteen.
	wp_enqueue_script( 'twentythirteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '2014-03-18', true );

	// Add Source Sans Pro and Bitter fonts, used in the main stylesheet.
	wp_enqueue_style( 'twentythirteen-fonts', twentythirteen_fonts_url(), array(), null );

	// Add Genericons font, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/fonts/genericons.css', array(), '2.09' );

	// Loads our main stylesheet.
	wp_enqueue_style( 'twentythirteen-style', get_stylesheet_uri(), array(), '2013-07-18' );

	// Loads the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'twentythirteen-ie', get_stylesheet_directory_uri() . '/css/ie.css', array( 'twentythirteen-style' ), '2013-07-18' );
	wp_style_add_data( 'twentythirteen-ie', 'conditional', 'lt IE 9' );
}
//remove_action( 'wp_enqueue_scripts', 'twentythirteen_scripts_styles' );
//add_action( 'wp_enqueue_scripts', 'twentythirteen_scripts_styles_child' );
add_filter('show_admin_bar', '__return_false');

function addImgsFromJson($filename, $dir, $addPermalink) {
  //put defines here:
  $headerWidth = 1040;
  $headerHeight = 230;
  $imgScale = 2;
  $foundPermalink = false;

          $dir = "/" . $dir . "/";
          $myFile = get_stylesheet_directory() . "/" . $filename;
	  $fh = fopen($myFile, 'r');
	  $theData = fread($fh, filesize($myFile));
	  fclose($fh);
	  $myData = json_decode($theData, true);
          $thePermalink = substr(get_permalink(), 1+strlen(get_bloginfo('url')));
          $thePermalink = substr($thePermalink,0,strlen($thePermalink)-1);
	  for ($i=0; $i < count($myData); $i++) {
	    if ((strpos($myData[$i]['id'],"Hover") === false) && 
		(strpos($myData[$i]['id'],"Selected") === false) &&
		(strpos($myData[$i]['id'],"Click") === false)) {
	      $id = $myData[$i]['id'];
              error_log($thePermalink);
              if (strcasecmp($thePermalink, $id) == 0) {
	        echo "<script>var buttonSelect = '{$id}';</script>";
	        error_log("found a match: ". $thePermalink);
	        $foundPermalink = true;
	      }
	      // main image
	      echo "<img id='". $id . "' ".
		      "class='headerImg' ".
		      "src='".get_stylesheet_directory_uri() . $dir . $id . ".png' ".
		      "draggable = false ".
		      "onload=f.createHotspot('".$myData[$i]['id']."',".
			$myData[$i]['width'].",".
			$myData[$i]['height']."); ".
		      "style='".
			"left:" . $myData[$i]['x']*100/$headerWidth . "%; ".
			"top:".$myData[$i]['y']*100/$headerHeight . "%; ".
			"width:".$myData[$i]['width'] * 100 / $imgScale / $headerWidth . "%; ".
			"height: auto; '".
		   " />";  

	      if (file_exists(get_stylesheet_directory() . $dir . $myData[$i]['id']."Hover.png")) {
		// hover, clickfrom, clickto, selected
		$states = array('Hover','ClickFrom','ClickTo','Selected');
		foreach ($states as &$state) {
		  if ($state !== ' ') {
		    $id = $myData[$i]['id'] . $state;
		    $headerImg = "headerImg needsclick headerImg" . $state;
		    echo "<img id='". $id . "' ".
			    "class='" . $headerImg ."' ".
			    "src='".get_stylesheet_directory_uri() . $dir . $id . ".png' ".
			    "draggable = false ".
			    "style='".
			      "left:" . $myData[$i]['x']*100/$headerWidth . "%; ".
			      "top:".$myData[$i]['y']*100/$headerHeight . "%; ".
			      "width:".$myData[$i]['width'] * 100 / $imgScale / $headerWidth . "%; ".
			      "height: auto; '".
		         " />";
		  }
		}
		
		// hotspot
		echo "<img id='".$myData[$i]['id']."Hotspot' ".
			"class='headerImgHotspot' ".
			"src='".get_stylesheet_directory_uri() . $dir . 
			$myData[$i]['id'] . ".png' ".
			"onmouseover=f.hover('".$myData[$i]['id']."'); " .
			"onmouseout=f.mouseout('".$myData[$i]['id']."'); " .
			//"onclick=f.click('".$myData[$i]['id']."'); " .
			// "onmousemove(e)=f.mousemove(e, '".$myData[$i]['id']."'); " .
			"draggable = false ".
			"style='".
			  "left:" . $myData[$i]['x']*100/$headerWidth . "%; ".
			  "top:".$myData[$i]['y']*100/$headerHeight . "%; ".
			  "width:".$myData[$i]['width'] * 100 / $imgScale / $headerWidth . "%; ".
			  "height: auto '".
		     " />";

		echo "<script>
      		  addListener(window, 'load', function() {
	            var id = document.getElementById('".$myData[$i]['id']."Hotspot');
         	    addListener(id, 'touchend', function(e) {
		      f.touch(e, '".$myData[$i]['id']."');
		      e.preventDefault();
	            }, false);
		    addListener(id, 'mousemove', function(e) {
		      f.mousemove(e,'".$myData[$i]['id']."');
		      e.preventDefault();
	            }, false);
		    addListener(id, 'click', function(e) {
		      f.click(e,'".$myData[$i]['id']."');
		      e.preventDefault();
		    }, false);
                  });
		</script>";
	      } // if
	    }
	  }
            if (($addPermalink) && ($foundPermalink === false)) {
              if (is_home()) {
                echo "<script>var buttonSelect = 'snapSays';</script>";
              } else {
                echo "<script>var buttonSelect = '';</script>";
              }
            }
}
?>
